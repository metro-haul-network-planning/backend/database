# database

The Back-end includes a database to store any information consumed and produced by the
algorithms. This permits the usage of historical data-sets that
are particularly useful to train machine-learning algorithms. 

## Topology 1
The topology, as depicted in the following figure, shows a mobile carrier network commonly modeled with a three-level hierarchical 
architecture, composed of the radio access, the back-haul, and the backbone networks.

<p align="center">
  <img src="topology-1/fig1.PNG" width="350" title="Fig. 1">
</p>

The base stations (BSs) deployed in the field provide radio access. Each BS (comprising a set of cell antennas and an eNodeB) is connected by a back-haul network segment to an aggregation node (AN). The ANs are the edge elements interfacing the back-haul to the backbone network. The backbone network is the infrastructure connecting the Access Nodes (ANs) to the serving gateway (SGW). We assumed that the metro backbone is divided into an aggregation and a metro-core segment, in order to gradually groom traffic of the metro area from the edges toward the SGW. The aggregation network is composed by metro optical rings, each one connecting a subset of neighbor ANs. On every ring, two ring nodes, called
interfacing nodes (INs), are used to interface the aggregation to the core segment of the backbone network. Each IN
is connected to a core node (CN) of the core network (Fig. 1). The mobile metro-core network is the mesh-topology fiber
infrastructure interconnecting the core nodes and the SGW. The assumption of mesh topology in the metro-core is consistent with the current evolutionary trend leading from ring to mesh in metro areas to reduce latency and increase reliability [34,35]. We suppose that each node of the core network is an optical cross connect (OXC). The SGW is connected to a packet gateway (PGW), which provides connectivity toward data center facilities and Internet exchange points (IXPs). It is important to note that all mobile data traffic must pass through the metro SGW; therefore, the part of a network that extends from the SGW to the PWG and beyond has not been included in this topology. 

The MCN topology was synthesized from the real geographical location of 2554 BSs in Milan [33], using the following methodology. 

1- RAN: Based on BSs location, a clustering algorithm creates groups of up to 15 BSs with minimum distance
between BSs and the AN of the cluster. 

2- Aggregation rings: Based on ANs, a second level of clustering creates eight aggregation rings of up to 20
ANs each. Each aggregation ring has two INs toward the metro-core.

3- Metro-core: It is composed of 16 (8 × 2) CNs and one SGW, connected by multi-fiber links (with 80 wavelengths per
fiber) in a maximal planar graph with 6 deg. 

Finally, we perform network dimensioning by solving a multi- commodity problem to minimize CapEx, assuming that every ring is generating traffic at its daily peak. The multi-commodity flow problem must be adapted to each model introduced in Section IV.B; however, to simplify the presentation, we do not describe them. The resulting network for the WP model is shown in the following figure.

<p align="center">
  <img src="topology-1/fig2.PNG" width="350" title="Fig. 2">
</p>


## Topology 2

Topology 2 consists of a 5-node network placed in Spain, see the following figure. Each link is equipped with a number of optical transponders allowing the use of a number of wavelengths per link each one operating at certain capacity in Gbps. Inside the folder topology-2/ there are all the information.

<p align="center">
  <img src="topology-2/fig3.png" width="350" title="Fig. 3">
</p>

## Topology 3

This topology represents a reference metro regional network topology with 52 nodes, out of which 2 nodes are metro core backbone (MCB) central offices, 6 nodes are metro core (MC) central offices and rest are metro access (MA) central offices. In this network there are 72 bidirectional WDM links (10 core and 62 extension links). Core links connect MC nodes to each otherand MCB nodes and are up to 200 km long. Extension links connect  MA  nodes  to  each  other  and  MCs  or  MCBs.

<p align="center">
  <img src=" topology-3/topology3.JPG" width="350" title="Topology3">
</p>
