# Source data


## BS_to_AG.csv 
It represents the aggregation (clustering) of Base Stations (BS) to AGgregation nodes (AG).
* Column 1: AG ID
* Column 2: BS ID

(made manually)

## corecoordinate.csv 
It represents the mapping between the AG ID and BS ID. 
For example: if a cluster is composed by BS ID's: [12, 3, 6, 9, 67, 98] for the AG ID: 1, then the BS ID selected as AG ID is the center of the BS ID's (such as 9). The center is derived taking into account the geo-spatial coordinates of the BS ID's.
* Column 1: central BS ID of the AG ID cluster
* Column 2: AG ID

(made manually)

## AG_to_ring.csv 
It represents the aggregation of AG ID to rings.
* Column 1: ring ID
* Column 2: AG ID

(made manually)

## location.csv 
It represents the geografical position of the BS's.
* Column 1: BS ID
* Column 2: longitutde

(made manually)

## planar_info.csv 
It represents the planar graph information.

(made by bin/0_Network_configuration/Planar_gen/Planar_gen.py)

## planar_graph 
It represents the planar graph edges information.

(made by bin/0_Network_configuration/Planar_gen/Planar_gen.py)

## position_file.pkl 
It represents the nodes geographical location information.

(made by bin/0_Network_configuration/Planar_gen/Planar_gen.py)

## position_info.csv 
It represents the node name and its geographical location information.

(made by bin/0_Network_configuration/Planar_gen/Planar_gen.py)

## color_list.pkl 
It represents the node color information.

(made by bin/0_Network_configuration/Planar_gen/Planar_gen.py)

## mobile_traffic_demands_milan.csv 
It represents the mobile traffic demands of each BS at each hour of the day for 62 day.
* Column 1: cell number
* Column 2 to N: sequence of Mbps at each hour.

(made manually)

## offline_mobile_traffic_demands_milan.csv 
It represents the mobile traffic demands of each BS at each hour of the day for 50 day.
* Column 1: cell number
* Column 2 to N: sequence of Mbps at each hour.

(made manually)

## online_mobile_traffic_demands_milan.csv 
It represents the mobile traffic demands of each BS at each hour of the day for 12 day.
* Column 1: cell number
* Column 2 to N: sequence of Mbps at each hour.

(made manually)

## MetroNetwork_milan.xlsx
Topology data with information on nodes, fibers, links, VNFs, etc.


